import { Component, OnInit } from "@angular/core";
import { productFixture } from "../../fixtures/products.fixture";

@Component({
  selector: "app-catalog",
  templateUrl: "./catalog.component.html",
  styleUrls: ["./catalog.component.scss"],
})
export class CatalogComponent implements OnInit {
  products;
  constructor() {
    this.products = productFixture;
  }

  ngOnInit(): void {}
}
