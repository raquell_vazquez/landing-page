import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { formInputs } from "../../fixtures/form.fixture";

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"],
})
export class FormComponent implements OnInit {
  /*
   * variable to  form group
   */
  form: FormGroup;
  /*
   * variable to text
   */
  staticText;
  /*
   * variable to  models
   */
  models;
  /*
   * constructor of the class
   */
  constructor() {
    this.buildForm();
    this.staticText = formInputs.labels;
    this.models = formInputs.models;
  }

  ngOnInit() {}
  /*
   * Methot to build reactive form
   */
  private buildForm() {
    this.form = new FormGroup({
      name: new FormControl("", [Validators.required]),
      email: new FormControl("", Validators.compose([Validators.email])),
      marca: new FormControl("", [Validators.required]),
    });
  }
  /*
   * save values
   */
  save(event: Event) {
    if (this.form.valid) {
      const value = this.form.value;
      this.form.reset();
      console.log(value);
      alert("Se ha enviado el correo");
    }
  }
}
