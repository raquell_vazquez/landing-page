import { Component, OnInit } from "@angular/core";
import { coverLabels } from "../../fixtures/cover.fixture";
@Component({
  selector: "app-cover",
  templateUrl: "./cover.component.html",
  styleUrls: ["./cover.component.scss"],
})
export class CoverComponent implements OnInit {
  /*
   * variable to text
   */
  staticText;
  constructor() {
    this.staticText = coverLabels.text;
  }

  ngOnInit(): void {}
}
