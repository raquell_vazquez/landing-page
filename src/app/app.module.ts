import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./components/header/header.component";
import { CoverComponent } from "./components/cover/cover.component";
import { FormComponent } from "./components/form/form.component";
import { ReactiveFormsModule } from "@angular/forms";
import { ProductComponent } from "./components/product/product.component";
import { CatalogComponent } from "./components/catalog/catalog.component";
import { FooterComponent } from "./components/footer/footer.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CoverComponent,
    FormComponent,
    ProductComponent,
    CatalogComponent,
    FooterComponent,
  ],
  imports: [BrowserModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
