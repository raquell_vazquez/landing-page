/**
 * text for cover
 */
export const coverLabels = {
  text: {
    title:
      "Obten un 15% de descuento en cualquiera de nuestros ultimos celulares seleccionados",
    description:
      "Esta es una oferta por tiempo limitado por el periodo identificado en la página de detalles del producto u otra página en conexión con la promoción.",
  },
};
