/**
 * initial configuration of the inputs
 */
export const formInputs = {
  labels: {
    name: "Nombre",
    email: "Correo",
    marca: "Modelo",
    save: "Guardar",
    copy:
      "Al llenar este formulario, te enviaremos un correo con el código de descuento para el modelo de celular que te interese.",
  },
  models: [
    {
      name: "Iphone 10",
    },
    {
      name: "Iphone SE",
    },
    {
      name: "Dogee s95",
    },
  ],
};
