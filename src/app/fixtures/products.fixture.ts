export const productFixture = [
  {
    image: "../assets/src/iphone_10.png",
    name: "Iphone 10",
    price: "$24,000",
    flagDisscount: true,
  },
  {
    image: "../assets/src/xiaomi_mid_10_pro.png",
    name: "Xiaomi 10",
    price: "$14,000",
    flagDisscount: false,
  },
  {
    image: "../assets/src/Iphone_SE.png",
    name: "Iphone SE",
    price: "$7,400",
    flagDisscount: true,
  },
  {
    image: "../assets/src/xiaomi_black_shark_3.png",
    name: "Xiaomi 3",
    price: "$6,000",
    flagDisscount: false,
  },
  {
    image: "../assets/src/Dogee_s95.png",
    name: "Dogee s95",
    price: "$4,000",
    flagDisscount: true,
  },
];
